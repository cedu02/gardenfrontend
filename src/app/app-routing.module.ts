import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'fan-control',
    pathMatch: 'full'
  },
  {
    path: 'fan-control',
    loadChildren: () => import('./fan-control/fan-control.module').then( m => m.FanControlPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'pump-control',
    loadChildren: () => import('./pump-control/pump-control.module').then( m => m.PumpControlPageModule)
  },
  {
    path: 'light-control',
    loadChildren: () => import('./light-control/light-control.module').then( m => m.LightControlPageModule)
  },
  {
    path: 'gallery',
    loadChildren: () => import('./gallery/gallery.module').then( m => m.GalleryPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
