import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {TranslateService} from '@ngx-translate/core';
import {TextKeys} from './core/i18n/TextKeys';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.translate.addLangs(['de']);
    this.translate.setDefaultLang('de');
    this.translate.use('de');
    this.translate.get(TextKeys.DashboardName).subscribe(name => this.appPages.push({
      title: name,
      url: '/dashboard',
      icon: 'leaf'
    }));
    this.translate.get(TextKeys.FanName).subscribe(name => this.appPages.push({
      title: name,
      url: '/fan-control',
      icon: 'medical'
    }));
    this.translate.get(TextKeys.PumpName).subscribe(name => this.appPages.push({
      title: name,
      url: '/pump-control',
      icon: 'water'
    }));
    this.translate.get(TextKeys.LightName).subscribe(name => this.appPages.push({
      title: name,
      url: '/light-control',
      icon: 'sunny'
    }));
    this.translate.get(TextKeys.GalleryName).subscribe(name => this.appPages.push({
      title: name,
      url: '/gallery',
      icon: 'videocam'
    }));
  }
}
