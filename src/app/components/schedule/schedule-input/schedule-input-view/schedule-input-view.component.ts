import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TextKeys} from '../../../../core/i18n/TextKeys';

@Component({
    selector: 'app-schedule-input-view',
    templateUrl: './schedule-input-view.component.html',
    styleUrls: ['./schedule-input-view.component.scss'],
})
export class ScheduleInputViewComponent implements OnInit {

    @Output() saveClicked = new EventEmitter<void>();

    texts = {
        interval: TextKeys.Schedule.interval,
        save: TextKeys.General.save
    };

    constructor() {
    }

    ngOnInit() {
    }

    onSaveClick() {
        this.saveClicked.emit();
    }
}
