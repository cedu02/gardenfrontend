import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {ScheduleInputComponent} from './schedule-input.component';
import {ScheduleInputViewComponent} from './schedule-input-view/schedule-input-view.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TranslateModule
    ],
    exports: [
        ScheduleInputViewComponent
    ],
    declarations: [ScheduleInputComponent,
        ScheduleInputViewComponent]
})
export class ScheduleInputModule {}
