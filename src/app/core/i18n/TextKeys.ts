export class TextKeys {
    public static FanStart = 'fan.start';
    public static FanStop = 'fan.stop';
    public static FanTitle = 'fan.title';
    public static FanName = 'fan.name';
    public static DashboardName = 'dashboard.name';
    public static DashboardVideo = 'dashboard.video';
    public static LightTitle = 'light.title';
    public static LightName = 'light.name';
    public static PumpTitle = 'pump.title';
    public static PumpName = 'pump.name';
    public static GalleryName = 'gallery.name';
    public static GalleryTitle = 'gallery.title';

    public static Schedule = {
        interval: 'schedule.interval'
    };

    public static General = {
        save: 'general.save'
    };
}
