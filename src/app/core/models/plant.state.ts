export interface PlantState {
    humidity: number;
    plantDate: Date;
    name: string;
    id: number;
}
