import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {AppConfig} from '../models/app.config';

@Injectable()
export class AppConfigService {
    static settings: AppConfig = {} as AppConfig;

    constructor(private http: HttpClient) {}

    load()  {
        const jsonFile = 'assets/config/config.json';
        return new Promise<void>((resolve, reject) => {
            this.http.get(jsonFile).toPromise().then((response: AppConfig) => {
                AppConfigService.settings = response as AppConfig;
                resolve();
            }).catch((response: any) => {
                reject(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
            });
        });
    }
}
