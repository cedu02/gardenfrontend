import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AppConfigService} from './app-config.service';

const controllerUrl = '/fan';

@Injectable({providedIn: 'root'})
export class FanService {
    private serviceUrl: string = AppConfigService.settings.baseUrl + controllerUrl;

    constructor(private httpClient: HttpClient) {}

    /**
     * Get Fan status from backend.
     */
    getFanStatus(): Observable<boolean> {
        return this.httpClient.get<boolean>(this.serviceUnnbnrl);
    }

    /**
     * Switch Fan to newState
     * @param newSate (true = fan on, false = fan off)
     */
    switchFan(newSate: boolean): Observable<boolean> {
        return this.httpClient.post<boolean>(this.serviceUrl, {});
    }
}
