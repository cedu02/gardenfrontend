// import { Injectable } from '@angular/core';
// import {BehaviorSubject, fromEvent, Observable, Subject} from 'rxjs';
// import {fromPromise} from 'rxjs/internal-compatibility';
// import {map, mapTo} from 'rxjs/operators';
// import {StreamUser} from '../models/stream.user';
//
// @Injectable({
//     providedIn: 'root'
// })
// export class RtcService {
//     public onNewOffer$ = new Subject<RTCSessionDescriptionInit>();
//     public onLocalDescription$ = new Observable<void>();
//     public onIceGatheringStateComplete$ = new Subject<RTCSessionDescription>();
//     private pc: RTCPeerConnection;
//
//     private users: BehaviorSubject<Array<StreamUser>>;
//     public users$: Observable<Array<StreamUser>>;
//
//     public data$ = new Subject<any>();
//
//     constructor() {
//         this.users = new BehaviorSubject([]);
//         this.users$ = this.users.asObservable();
//         // this.start();
//     }
//
//     public newUser(user: StreamUser): void {
//         this.users.next([...this.users.getValue(), user]);
//     }
//
//     negotiate() {
//         this.pc.addTransceiver('video', {direction: 'recvonly'});
//         this.pc.addTransceiver('audio', {direction: 'recvonly'});
//
//         fromPromise(this.pc.createOffer()).subscribe(offer => {
//             console.log('create offer');
//             this.onNewOffer$.next(offer);
//         });
//
//         this.onNewOffer$.asObservable().subscribe(offer => {
//             console.log('created offer', offer);
//             this.pc.setLocalDescription(offer).then(() => {
//                     console.log('ice gathering');
//                     if (this.pc.iceGatheringState === 'complete') {
//                         console.log('ice gathering complete');
//                         this.onIceGatheringStateComplete$.next(this.pc.localDescription);
//                     } else {
//                         console.log('wait for ice gathering state change');
//                         fromEvent(this.pc, 'icegatheringstatechange').subscribe(() => {
//                             console.log('ice gathering state changed to:', this.pc.iceGatheringState);
//                             if (this.pc.iceGatheringState === 'complete') {
//                                 this.onIceGatheringStateComplete$.next(this.pc.localDescription);
//                             }
//                         });
//                     }
//             });
//         });
//     }
//
//     setAnswer(answer) {
//         this.pc.setRemoteDescription(JSON.parse(answer.signal)).then(() => console.log('Set remote description'));
//     }
//
//     start() {
//         const config: RTCConfiguration = {
//             sdpSemantics: 'unified-plan'
//         } as RTCConfiguration;
//         config.iceServers = [{urls: ['stun:stun.l.google.com:19302']}];
//         this.pc = new RTCPeerConnection(config);
//         // connect audio / video
//         this.pc.addEventListener('track', (evt) => {
//             if (evt.track.kind === 'video') {
//                 console.log('stream arrived');
//                 this.data$.next(evt.streams[0]);
//             } else {
//                 let test = evt.streams[0];
//                 console.log('stream arrived');
//             }
//         });
//         this.negotiate();
//     }
// }
