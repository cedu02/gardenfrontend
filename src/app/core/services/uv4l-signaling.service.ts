import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class Uv4lSignalingService {
    RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection;
    RTCSessionDescription = window.RTCSessionDescription;
    RTCIceCandidate = window.RTCIceCandidate;
    ws: WebSocket;

    signal(url, onStream, onError, onClose, onMessage) {
        if ('WebSocket' in window) {
            console.log('opening web socket: ' + url);
            this.ws = new WebSocket(url);
            let pc;
            let iceCandidates = [];
            let hasRemoteDesc = false;

            function addIceCandidates() {
                if (hasRemoteDesc) {
                    iceCandidates.forEach((candidate) => {
                        pc.addIceCandidate(candidate,
                            () => {
                                console.log('IceCandidate added: ' + JSON.stringify(candidate));
                            },
                            (error) => {
                                console.error('addIceCandidate error: ' + error);
                            }
                        );
                    });
                    iceCandidates = [];
                }
            }

            this.ws.onopen = () => {
                /* First we create a peer connection */
                const config = {iceServers: [{urls: ['stun:stun.l.google.com:19302']}]};
                pc = new RTCPeerConnection(config);

                iceCandidates = [];
                hasRemoteDesc = false;

                pc.onicecandidate = (event) => {
                    if (event.candidate) {
                        const candidate = {
                            sdpMLineIndex: event.candidate.sdpMLineIndex,
                            sdpMid: event.candidate.sdpMid,
                            candidate: event.candidate.candidate
                        };
                        this.ws.send(JSON.stringify({
                            what: 'addIceCandidate',
                            data: JSON.stringify(candidate)
                        }));
                    } else {
                        console.log('end of candidates.');
                    }
                };

                if ('ontrack' in pc) {
                    pc.ontrack = (event) => {
                        onStream(event.streams[0]);
                    };
                } else {  // onaddstream() deprecated
                    pc.onaddstream = (event) => {
                        onStream(event.stream);
                    };
                }

                pc.onremovestream = (event) => {
                    console.log('the stream has been removed: do your stuff now');
                };

                pc.ondatachannel = (event) => {
                    console.log('a data channel is available: do your stuff with it');
                    // For an example, see https://www.linux-projects.org/uv4l/tutorials/webrtc-data-channels/
                };

                /* kindly signal the remote peer that we would like to initiate a call */
                const request = {
                    what: 'call',
                    options: {
                        // If forced, the hardware codec depends on the arch.
                        // (e.g. it's H264 on the Raspberry Pi)
                        // Make sure the browser supports the codec too.
                        force_hw_vcodec: true,
                        vformat: 30, /* 30=640x480, 30 fps */
                        trickle_ice: true
                    }
                };
                console.log('send message ' + JSON.stringify(request));
                this.ws.send(JSON.stringify(request));
            };

            this.ws.onmessage = (evt) => {
                const msg = JSON.parse(evt.data);
                const what = msg.what;
                const data = msg.data;

                console.log('received message ' + JSON.stringify(msg));

                switch (what) {
                    case 'offer':
                        const mediaConstraints = {
                            optional: [],
                            mandatory: {
                                OfferToReceiveAudio: true,
                                OfferToReceiveVideo: true
                            }
                        };
                        pc.setRemoteDescription(new RTCSessionDescription(JSON.parse(data)),
                            function onRemoteSdpSuccess() {
                                hasRemoteDesc = true;
                                addIceCandidates();
                                pc.createAnswer((sessionDescription) => {
                                    pc.setLocalDescription(sessionDescription);
                                    const request = {
                                        what: 'answer',
                                        data: JSON.stringify(sessionDescription)
                                    };
                                    this.ws.send(JSON.stringify(request));
                                }, (error) => {
                                    onError('failed to create answer: ' + error);
                                }, mediaConstraints);
                            },
                            function onRemoteSdpError(event) {
                                onError('failed to set the remote description: ' + event);
                                this.ws.close();
                            }
                        );

                        break;

                    case 'answer':
                        break;

                    case 'message':
                        if (onMessage) {
                            onMessage(msg.data);
                        }
                        break;

                    case 'iceCandidate': // received when trickle ice is used (see the "call" request)
                        if (!msg.data) {
                            console.log('Ice Gathering Complete');
                            break;
                        }
                        const elt = JSON.parse(msg.data);
                        const candidate = new RTCIceCandidate({sdpMLineIndex: elt.sdpMLineIndex, candidate: elt.candidate});
                        iceCandidates.push(candidate);
                        addIceCandidates(); // it internally checks if the remote description has been set
                        break;

                    case 'iceCandidates': // received when trickle ice is NOT used (see the "call" request)
                        const candidates = JSON.parse(msg.data);
                        for (let i = 0; i > candidates.length; i++) {
                            iceCandidates.push(new RTCIceCandidate({
                                sdpMLineIndex: candidates[i].sdpMLineIndex,
                                candidate: candidates[i].candidate
                            }));
                        }
                        addIceCandidates();
                        break;
                }
            };

            this.ws.onclose = (event) => {
                console.log('socket closed with code: ' + event.code);
                if (pc) {
                    pc.close();
                    pc = null;
                    this.ws = null;
                }
                if (onClose) {
                    onClose();
                }
            };

            this.ws.onerror = (event) => {
                onError('An error has occurred on the websocket (make sure the address is correct)!');
            };


        } else {
            onError('Sorry, this browser does not support Web Sockets. Bye.');
        }
    }

    hangup() {
        if (this.ws) {
            const request = {
                what: 'hangup'
            };
            console.log('send message ' + JSON.stringify(request));
            this.ws.send(JSON.stringify(request));
        }
    }
}
