import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AppConfigService} from './app-config.service';

const controllerUrl = '/pump';

@Injectable({providedIn: 'root'})
export class FanService {
    private serviceUrl: string = AppConfigService.settings.baseUrl + controllerUrl;

    constructor(private httpClient: HttpClient) {}

    /**
     * Get pump status from backend.
     */
    getPumpStatus(): Observable<boolean> {
        return this.httpClient.get<boolean>(this.serviceUrl);
    }

    /**
     * Switch pump to newState
     * @param newSate (true = fan on, false = fan off)
     */
    switchPump(newSate: boolean): Observable<boolean> {
        return this.httpClient.post<boolean>(this.serviceUrl, {});
    }
}
