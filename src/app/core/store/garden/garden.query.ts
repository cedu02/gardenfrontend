import { Query } from '@datorama/akita';
import {GardenState, GardenStore} from './garden.store';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class GardenQuery extends Query<GardenState> {
    fan$ = this.select(state => state.fans);
    waterPump$ = this.select(state => state.waterPump);
    light$ = this.select(state => state.light);

    constructor(protected store: GardenStore) {
        super(store);
    }
}
