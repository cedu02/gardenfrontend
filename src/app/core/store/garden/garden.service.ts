import {Injectable} from '@angular/core';
import {GardenStore} from './garden.store';
import {FanService} from '../../services/fan.service';

@Injectable({providedIn: 'root'})
export class GardenService {
    constructor(private gardenStore: GardenStore,
                private fanService: FanService) {}

    /**
     * Get Fan status from Backend and update store.
     */
    getFanStatus(): void {
        this.fanService.getFanStatus().subscribe(
            fanState => this.gardenStore.update(state => ({
                ...state,
                fans: fanState
            }))
        );
    }

    /**
     * Set Fan to newState and update store.
     * @param newState (false = fan off, true = fan on)
     */
    switchFan(newState: boolean): void {
        this.fanService.switchFan(newState).subscribe(
            fanState => this.gardenStore.update(state => ({
                ...state,
                fans: fanState
            }))
        );
    }
}
