import { Store, StoreConfig } from '@datorama/akita';
import {Injectable} from '@angular/core';
import {PlantState} from '../../models/plant.state';

export interface GardenState {
    fans: boolean;
    waterPump: boolean;
    light: boolean;
    waterLevelTray: number;
    waterLevelReservoir: number;
    temperaturePlant: number;
    temperatureWater: number;
    humidityAir: number;
    phLevelWater: number;
    tdsWater: number;
    plants: PlantState[];
}

export function createInitialState(): GardenState {
    return {
        fans: false,
        waterPump: false,
        light: false,
        waterLevelTray: 0,
        waterLevelReservoir: 0,
        temperaturePlant: 0,
        temperatureWater: 0,
        humidityAir: 0,
        phLevelWater: 0,
        tdsWater: 0,
        plants: []
    };
}

@Injectable({providedIn: 'root'})
@StoreConfig({ name: 'session' })
export class GardenStore extends Store<GardenState> {
    constructor() {
        super(createInitialState());
    }
}
