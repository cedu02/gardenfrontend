import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TextKeys} from '../core/i18n/TextKeys';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit, OnDestroy {
  @ViewChild('videoPlayer', { static: false }) videoPlayer: ElementRef;

  textKeys =  {
    dashboardTitle: TextKeys.DashboardName,
  };
  subscriptions = new Subscription();

  constructor() { }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
