import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FanControlPage } from './fan-control.page';

const routes: Routes = [
  {
    path: '',
    component: FanControlPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FanControlPageRoutingModule {}
