import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FanControlPageRoutingModule } from './fan-control-routing.module';

import { FanControlPage } from './fan-control.page';
import {TranslateModule} from '@ngx-translate/core';
import {ScheduleInputModule} from '../components/schedule/schedule-input/schedule-input.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FanControlPageRoutingModule,
        TranslateModule,
        ScheduleInputModule
    ],
  declarations: [FanControlPage]
})
export class FanControlPageModule {}
