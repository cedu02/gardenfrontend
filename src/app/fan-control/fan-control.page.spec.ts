import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FanControlPage } from './fan-control.page';

describe('FanControlPage', () => {
  let component: FanControlPage;
  let fixture: ComponentFixture<FanControlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FanControlPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FanControlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
