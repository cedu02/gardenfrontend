import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import { AnimationController } from '@ionic/angular';
import {GardenQuery} from '../core/store/garden/garden.query';
import {GardenService} from '../core/store/garden/garden.service';
import {Subscription} from 'rxjs';
import {TextKeys} from '../core/i18n/TextKeys';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-fan-control',
  templateUrl: './fan-control.page.html',
  styleUrls: ['./fan-control.page.scss'],
})
export class FanControlPage implements OnInit, AfterViewInit, OnDestroy {
  private animation;
  private subscriptions: Subscription[] = [];
  public textKeys =  {
    fanStart: TextKeys.FanStart,
    fanStop: TextKeys.FanStop,
    fanTitle: TextKeys.FanTitle
  };
  fanActive$ = this.gardenQuery.fan$;

  constructor(private animationCtrl: AnimationController,
              private gardenQuery: GardenQuery,
              private gardenService: GardenService) {}

  /**
   * Create Animation for Fan image.
   */
  ngOnInit() {
    this.animation = this.animationCtrl.create()
        .addElement(document.querySelector('.fan-image'))
        .duration(2000)
        .iterations(Infinity)
        .keyframes([
          { offset: 0, transform: 'rotate(0)' },
          { offset: 1, transform: 'rotate(360deg)' },
        ]);
  }

  /**
   * Register subscription to stop and start fan animation abridging to global state.
   */
  ngAfterViewInit(): void {
    // Start and stop animation with changes of fan$
    this.subscriptions.push(this.gardenQuery.fan$.subscribe(state => {
      if (state) {
        this.startAnimation();
      } else {
        this.stopAnimation();
      }
    }));
  }

  /**
   * Unsubscribe from all subscriptions.
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Handles fan button pressed event.
   */
  switchFan(): void {
    this.gardenQuery.fan$
        .pipe(
            take(1))
        .subscribe(state => this.gardenService.switchFan(!state));
  }

  private stopAnimation(): void {
    this.animation.stop();
  }

  private startAnimation(): void {

    this.animation.play();
  }
}
