import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LightControlPage } from './light-control.page';

describe('LightControlPage', () => {
  let component: LightControlPage;
  let fixture: ComponentFixture<LightControlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LightControlPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LightControlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
