import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PumpControlPage } from './pump-control.page';

const routes: Routes = [
  {
    path: '',
    component: PumpControlPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PumpControlPageRoutingModule {}
