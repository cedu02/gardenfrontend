import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PumpControlPageRoutingModule } from './pump-control-routing.module';

import { PumpControlPage } from './pump-control.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PumpControlPageRoutingModule
  ],
  declarations: [PumpControlPage]
})
export class PumpControlPageModule {}
