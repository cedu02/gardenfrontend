import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PumpControlPage } from './pump-control.page';

describe('PumpControlPage', () => {
  let component: PumpControlPage;
  let fixture: ComponentFixture<PumpControlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PumpControlPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PumpControlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
